package com.example.jdbcsource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties({QueryProperties.class, QueryProperties.QueryDataSourceProperties.class})
public class QueryDataSourceConfiguration {

    @Bean(name = "queryDataSource")
    public DataSource dataSource(final QueryProperties.QueryDataSourceProperties dataSourceProperties) {
        return DataSourceBuilder.create()
                .url(dataSourceProperties.getUrl())
                .driverClassName(dataSourceProperties.getDriverClassName())
                .username(dataSourceProperties.getUsername())
                .password(dataSourceProperties.getPassword())
                .build();
    }

    @Bean(name = "queryJdbcTemplate")
    public JdbcTemplate jdbcTemplate(@Qualifier("queryDataSource") final DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
