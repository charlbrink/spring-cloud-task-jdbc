package com.example.jdbcsource;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "query")
public class QueryProperties {

    private String select;

    @Data
    @ConfigurationProperties(prefix = "query.datasource")
    public class QueryDataSourceProperties {

        private String driverClassName;
        private String url;
        private String username;
        private String password;

    }

}
