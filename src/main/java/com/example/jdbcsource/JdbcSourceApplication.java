package com.example.jdbcsource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

@Slf4j
@EnableTask
@SpringBootApplication
public class JdbcSourceApplication {
	@Bean
	public CommandLineRunner replenishTask() {
		return new ReplenishTask();
	}

	public static void main(String[] args) {
		SpringApplication.run(JdbcSourceApplication.class, args);
	}

	/**
	 * A commandline runner that prints a timestamp.
	 */
	public static class ReplenishTask implements CommandLineRunner {

		@Autowired
		private QueryProperties queryProperties;

		@Qualifier("queryJdbcTemplate")
		@Autowired
		private JdbcTemplate jdbcTemplate;

		@Override
		public void run(final String... strings) throws Exception {
			Integer count = jdbcTemplate.queryForObject(queryProperties.getSelect(), Integer.class);
			log.info("Replenished reference numbers [{}]", count);
		}

	}

}
