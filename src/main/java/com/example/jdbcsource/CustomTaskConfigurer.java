package com.example.jdbcsource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.task.configuration.DefaultTaskConfigurer;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class CustomTaskConfigurer extends DefaultTaskConfigurer {

    public CustomTaskConfigurer(@Qualifier("taskDataSource") final DataSource dataSource) {
        super(dataSource);
    }

}

